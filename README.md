


# Wifi outage bash script

## Bash script to switch to backup wifi during outage

Bash script to switch to backup wifi during outage

Inspired by: https://github.com/rwbaumg/admin-scripts/blob/master/net/isup.sh

And here: https://gist.github.com/gtfunes/58c1de6eca94f3038b6cba207a0b95bf

Required: 

This script connects to the secondary wifi hotspot when there is an outage
```
su -                                                         ~ superuser is doing these tasks!
chmod +x /usr/local/bin/wifi-outage
EDITOR=vi && crontab -e
*/3 * * * * /usr/local/bin/wifi-outage
```
```
Output:
[jeremy-manj bin]# ./wifi-outage 
Loading known networks list...
        AC:86:74:32:50:CB  SSID PVT          Infra  5     65 Mbit/s   100     ▂▄▆█  WPA2     
        FA:8F:CA:9D:67:4E  SSID              Infra  5     65 Mbit/s   74      ▂▄▆_  --       
        44:A5:6E:62:75:32  SSID              Infra  36    270 Mbit/s  74      ▂▄▆_  WPA2     
*       AC:86:74:32:50:CA  SSID              Infra  5     65 Mbit/s   72      ▂▄▆_  WPA2     
     
        92:30:D9:DC:D1:55  --                Infra  100   540 Mbit/s  29      ▂___  WPA2     
Loaded!

CURRENT_NETWORK is: 0 :  "SSID"
Running on wlan0 adapter
SECS is: 1
Interval is set to 1
Total networks is 2
Connecting to network "SSID"
Connected!

SECS outside while loop is: 0
SECS inside while loop is: 1
sending ping test inside func!
Switching networks... as ploss variable equals: 10
```

## Bash script 2 to connect to secondary mobile hotspot wifi

This script periodically connects else the hotspot turns itself off due to inactivity
```
su -                                                         ~ superuser is doing these tasks!
chmod +x /usr/local/bin/switch-wifi
EDITOR=vi && crontab -e
*/20 * * * * /usr/local/bin/switch-wifi
```
```
Output: [jeremy-manj temp]# journalctl -rt switch-wifi --boot
Nov 02 22:09:12 jeremy-manj switch-wifi[38863]: Connection is still up on "SSID"!
Nov 02 22:08:45 jeremy-manj switch-wifi[38789]: Inside func and connecting to network "SSID"
Nov 02 22:08:45 jeremy-manj switch-wifi[38787]: Connecting to network "SSID"
Nov 02 22:08:45 jeremy-manj switch-wifi[38785]: Total networks is 2
Nov 02 22:08:45 jeremy-manj switch-wifi[38783]: Total networks is 2
Nov 02 22:08:45 jeremy-manj switch-wifi[38781]: MAX_SECS Interval is set to 1
Nov 02 22:08:45 jeremy-manj switch-wifi[38772]: SECS is: 1
Nov 02 22:08:45 jeremy-manj switch-wifi[38770]: CURRENT_NETWORK is: 0
Nov 02 22:05:02 jeremy-manj switch-wifi[38550]: Inside func and connecting to network ""
Nov 02 22:05:02 jeremy-manj switch-wifi[38548]: Connecting to network ""
Nov 02 22:05:02 jeremy-manj switch-wifi[38546]: Total networks is 0
Nov 02 22:05:02 jeremy-manj switch-wifi[38544]: Total networks is 0
Nov 02 22:05:02 jeremy-manj switch-wifi[38542]: MAX_SECS Interval is set to 1
Nov 02 22:05:02 jeremy-manj switch-wifi[38540]: Running on interface adapter
Nov 02 22:05:02 jeremy-manj switch-wifi[38531]: SECS is: 1
Nov 02 22:05:02 jeremy-manj switch-wifi[38529]: CURRENT_NETWORK is: 0

```
```
Resolving error:
Error: Connection activation failed: (60) New connection activation was enqueued.
Can workaround this by manually switching until NetworkManager finds networks OK!:
[jeremy-manj bin]# nmcli dev wifi connect SSID
Error: Connection activation failed: (60) New connection activation was enqueued.
[jeremy-manj bin]# nmcli dev wifi connect SSID
Error: No network with SSID 'GSMI_wireless' found.
[jeremy-manj bin]# nmcli dev wifi connect SSID
Error: Connection activation failed: (60) New connection activation was enqueued.
[jeremy-manj bin]# nmcli dev wifi connect SSID
Device 'wlan0' successfully activated with '333079dc-0c48-4e08-accb-f23718e912d7'.
[jeremy-manj bin]# nmcli dev wifi connect SSID
Device 'wlan0' successfully activated with '3a6102a8-bd42-4452-ab61-3f71723cbfea'.
```
